﻿using System;

namespace Phoenix.OnlineBanking.BankMonitoringTool.Core.DTOs
{
    public class Account
    {
        public string Balance { get; set; }

        public string AvailableBalance { get; set; }

        public string Description { get; set; }

        internal string Type { get; set; }

        public string AccountNumber { get; set; }

        public bool BankStatementsDownloadable { get; set; }

        public Guid AccountId { get; set; }

        internal int Order { get; set; }

        public Account()
        {

        }
    }
}
