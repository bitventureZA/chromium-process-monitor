﻿using System;
using System.Collections.Generic;

namespace Phoenix.OnlineBanking.BankMonitoringTool.Core.DTOs
{
    public class LoginResponse
    {
        public bool LoginSuccessful { get; set; }

        public Guid SessionId { get; set; }

        public bool ServiceUnavailable { get; set; }

        public bool TermsAndConditions { get; set; }

        public List<Account> Accounts { get; set; }

        public LoginResponse()
        {
            Accounts = new List<Account>();
        }
    }
}
