﻿namespace Phoenix.OnlineBanking.BankMonitoringTool.Core.DTOs.Bank
{
    public class FNBRequest
    {
        public string bank { get; set; }

        public string username { get; set; }

        public string password { get; set; }
    }

    public class ABSARequest
    {
        public string bank { get; set; }

        public string accountNumber { get; set; }

        public string pin { get; set; }

        public string userNumber { get; set; }

        public string absolutePassword { get; set; }
    }

    public class NedbankOldRequest
    {
        public string bank { get; set; }

        public OldProfile profile  { get; set; }

    }

    public class NedbankNewRequest
    {
        public string bank { get; set; }

        public NewProfile profile { get; set; }
    }

    public class CapitecRequest
    {
        public string bank { get; set; }

        public string accountNumber { get; set; }

        public string password { get; set; }
    }

    public class StandardbankRequest
    {
        public string bank { get; set; }

        public string email { get; set; }

        public string password { get; set; }
    }

    public class OldProfile
    {
        public string profileType { get; set; }

        public string accountNumber { get; set; }

        public string accountPin { get; set; }

        public string password { get; set; }
    }

    public class NewProfile
    {
        public string profileType { get; set; }

        public string username { get; set; }

        public string password { get; set; }
    }
}
