﻿using static Phoenix.OnlineBanking.BankMonitoringTool.Core.Enum.Enum;

namespace Phoenix.OnlineBanking.BankMonitoringTool.Core.Constants
{
    public static class FNBCredentials
    {
        public static string bank = Bank.FNB.ToString();

        public static string Username = "Sm9yZGFuS3lsZUJyaXRz";

        public static string Password = "UmVhbE1hZHJpZDA4MjMqIw==";
    }

    public static class ABSACredentials
    {
        public static string bank = Bank.ABSA.ToString();

        public static string AccountNumber = "NDA3MzUzNzgxNg==";

        public static string Pin = "MTU5NzM=";

        public static string UserNumber = "MQ==";

        public static string AbsolutePassword = "ODIxN0RlbWJv";
    }

    public static class NedbankOldCredentials
    {
        public static string bank = Bank.NedBank.ToString();

        public static OldProfile Profile;
    }

    public static class NedbankIDCredentials 
    {
        public static string bank = Bank.NedBank.ToString();

        public static NewProfile Profile;
    }

    public static class CapitecCredentials
    {
        public static string bank = Bank.Capitec.ToString();

        public static string AccountNumber = "MTY3MjU3ODk2Mw==";

        public static string Password = "MDU5OQ==";
    }

    public static class StandardbankCredntials
    {
        public static string bank = Bank.StandardBank.ToString();

        public static string Email = "anVuaW9yQGJpdHZlbnR1cmUuY28uemE=";

        public static string Password = "ODIxN0RlbWJv";
    }

    public class OldProfile
    {
        public  string ProfileType = Nedbank.Old.ToString();

        public  string AccountNumber = "MzA2MTA5NzE1Mg==";

        public  string AccountPin = "MTkwMQ==";

        public  string Password = "MTkxOThkaA==";
    }

    public  class NewProfile
    {
        public  string ProfileType = Nedbank.New.ToString();

        public  string Username = "REhlcmJlcnQ=";

        public  string Password = "Q29ubmVjdDg4QA==";
    }
}
