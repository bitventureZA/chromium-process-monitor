﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.OnlineBanking.BankMonitoringTool.Core.Constants
{
    public static class URLs
    {
        public static string AUTH_SERVER_REQUEST_URL = "https://uat.bitventure.co.za/auth/connect/token";

        public static string Login = "https://uat.bitventure.co.za/OnlineBanking/BankLogin";
    }

    public static class MediaType
    {
        public static string APPLICATION_JSON = "application/json";
    }

    public static class AuthenticationHeaders
    {
        public static string GRANT_TYPE = "grant_type";
        public static string USERNAME = "username";
        public static string PASSWORD = "password";
        public static string SCOPE = "scope";
        public static string CLIENT_ID = "client_id";
        public static string CLIENT_SECRET = "client_secret";
    }
}
