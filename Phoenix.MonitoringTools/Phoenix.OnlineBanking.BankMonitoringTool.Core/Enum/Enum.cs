﻿using System.Runtime.Serialization;

namespace Phoenix.OnlineBanking.BankMonitoringTool.Core.Enum
{
    public class Enum
    {
        public enum Bank
        {
            [EnumMember(Value = "ABSA")]
            ABSA,
            [EnumMember(Value = "FNB")]
            FNB,
            [EnumMember(Value = "CAPITEC")]
            Capitec,
            [EnumMember(Value = "NEDBANK")]
            NedBank,
            [EnumMember(Value = "StandardBank")]
            StandardBank
        }

        public enum Nedbank
        {
            [EnumMember(Value = "Old")]
            Old,
            [EnumMember(Value = "New")]
            New
        }
    }
}
