﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Models;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Repositories
{
    public interface ISessionRepository
    {
        Task<IEnumerable<Sessions>> SelectAllBankSessions(IDbTransaction transaction);

        Task<IEnumerable<int?>> UpdateSessionTable(IDbTransaction transaction, Sessions bank);
    }

    public class SessionRepository : ISessionRepository
    {
        #region SQL Queries
        private static string SELECT_BANK_SESSIONS = @"SELECT 
                                                        i.Code, s.Url, sar.[Order]
                                                    FROM 
                                                        Session s (NOLOCK)
                                                        JOIN Institution i (NOLOCK) ON i.ID = s.InstitutionID
                                                        JOIN SessionAccountReference sar (NOLOCK) ON sar.SessionID = s.ID
                                                    WHERE 
                                                        s.Active = 1 AND 
                                                        sar.[Order] = 1";

        private static string UPDATE_SESSIONS_TABLE = @"UPDATE 
                                                         sn 
                                                     SET 
                                                         sn.[Active] = '0' 
                                                     FROM
                                                         [Session] sn
                                                     WHERE 
                                                         sn.[Url] = @PuppeteerUrl";
        #endregion

        public async Task<IEnumerable<Sessions>> SelectAllBankSessions(IDbTransaction transaction)
        { 
            return await transaction.Connection.QueryAsync<Sessions>(SELECT_BANK_SESSIONS, transaction: transaction);
        }

        public async Task<IEnumerable<int?>> UpdateSessionTable(IDbTransaction transaction, Sessions bank)
        {
            return await transaction.Connection.QueryAsync<int?>(UPDATE_SESSIONS_TABLE, transaction: transaction);
        }
    }
}
