﻿using System.Runtime.Serialization;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Enums
{
    public class Enum
    {
        public enum Bank
        {
            [EnumMember(Value = "ABSA")]
            ABSA,
            [EnumMember(Value = "FNB")]
            FNB,
            [EnumMember(Value = "CAPITEC")]
            Capitec,
            [EnumMember(Value = "NEDBANK")]
            NedBank,
            [EnumMember(Value = "SBSA")]
            StandardBank
        }
    }
}
