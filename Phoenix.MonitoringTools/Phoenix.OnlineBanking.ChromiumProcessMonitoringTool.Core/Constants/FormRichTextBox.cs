﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Constants
{
    public class FormRichTextBox : TextWriter
    {
        private readonly RichTextBox TextBox;

        private readonly int MaxLogLines = 1000;
        private readonly string LogPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\.logs";

        public FormRichTextBox(RichTextBox richtb)
        {
            TextBox = richtb;
        }

        private void CaptureMessage(string Message)
        {
            TextBox.Text += Message + Environment.NewLine;
            TextBox.SelectionStart = TextBox.Text.Length;
            TextBox.ScrollToCaret();

            FlushLog();
        }

        public override void WriteLine(string value)
        {
            if (TextBox.InvokeRequired)
            {
                TextBox.BeginInvoke(new DoUIWorkHandler(this.CaptureMessage), new object[] { value });
            }
            else
            {
                CaptureMessage(value);
            }
        }

        public override Encoding Encoding
        {
            get { return Encoding.ASCII; }
        }

        public void FlushLog(bool Force = false)
        {
            if ((TextBox.Lines.Length > MaxLogLines) || Force)
            {
                string FileName = LogPath + "\\" + DateTime.Now.ToString("yyyyMMdd") + ".log";

                Directory.CreateDirectory(Path.GetDirectoryName(FileName));

                using (StreamWriter writer = new StreamWriter(FileName, true))
                    writer.WriteLine(TextBox.Text);

                TextBox.Text = String.Empty;
            }
        }

        public delegate void DoUIWorkHandler(string Message);
    }
}
