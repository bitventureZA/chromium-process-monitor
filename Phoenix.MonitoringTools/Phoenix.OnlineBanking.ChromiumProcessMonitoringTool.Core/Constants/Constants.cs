﻿using System.Configuration;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Constants
{
    public class Constants
    {
        public const string IP = "127.0.0.1";

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["OnlineBanking"].ToString();
    }
}
