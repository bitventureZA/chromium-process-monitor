﻿using static Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Enums.Enum;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.DTOs
{
    public class ChromiumProcessDTO
    {
        public string BankCode { get; set; }

        public string PuppeteerURL { get; set; }

        internal Bank Bank { get; set; }
    }
}
