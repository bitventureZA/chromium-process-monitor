﻿using static Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Enums.Enum;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Models
{
    public class Sessions
    {
        public string BankCode { get; set; }

        public string PuppeteerURL { get; set; }

        internal Bank Bank { get; set; }
    }
}
