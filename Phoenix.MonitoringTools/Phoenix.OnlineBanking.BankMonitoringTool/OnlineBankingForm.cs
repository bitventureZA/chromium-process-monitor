﻿using Phoenix.OnlineBanking.BankMonitoringTool.Core.Constants;
using Phoenix.OnlineBanking.BankMonitoringTool.Processors;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phoenix.OnlineBanking.BankMonitoringTool
{
    public partial class OnlineBankingForm : Form
    {
        private IScheduler scheduler;
        private static FormRichTextBox Writer;

        public OnlineBankingForm()
        {
            InitializeComponent();

            Writer = new FormRichTextBox(rtbLog);
            Console.SetOut(Writer);

            GetSchedulers();
        }

        //Gets the Scheduler
        public async void GetSchedulers()
        {
            scheduler = await (new StdSchedulerFactory().GetScheduler());
        }

        public void ConfigureAndStartScheduler()
        {
            scheduler.Clear();

            ISet<ITrigger> Logout = new HashSet<ITrigger>();

            IJobDetail BankLogout = JobBuilder.Create<UpdateLogoutJob>()
                .WithIdentity("UpdateLogoutJob", "UpdateBankLogouts")
                .Build();

            ITrigger UpdateLogoutTrigger = TriggerBuilder.Create()
                .WithIdentity("UpdateLogoutTrigger", "UpdateBankLogouts")
                .WithCronSchedule("0 0/1 * 1/1 * ? *")
                .Build();

            Logout.Add(UpdateLogoutTrigger);

            scheduler.Start();

            scheduler.ScheduleJob(BankLogout, UpdateLogoutTrigger);

            Writer.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "]" + " Monitoring Tool Started.");
        }

        //The Job it must execute
        [DisallowConcurrentExecution]
        public class UpdateLogoutJob : IJob
        {
            public async Task Execute(IJobExecutionContext context)
            {
                try
                {
                    
                }
                catch (Exception ex)
                {
                    await ErrorMessage(ex);
                }
            }
        }

        //Log info Message
        public static async Task InfoMessage(string value)
        {
            Writer.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "] " + value);
        }

        //Log Error Message
        public static async Task ErrorMessage(Exception value)
        {
            Writer.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "] " + value);
        }

        //FNB
        private async void button1_Click(object sender, EventArgs e)
        {
            await InfoMessage("Process Started for FNB login");

            OnlineBankingProcessor onlineBankingProcessor = new OnlineBankingProcessor();

            await onlineBankingProcessor.FNBLogin();
        }

        //ABSA
        private async void button2_Click(object sender, EventArgs e)
        {
            OnlineBankingProcessor onlineBankingProcessor = new OnlineBankingProcessor();

            await InfoMessage("Process Started for ABSA login");

            await onlineBankingProcessor.ABSALogin();
        }

        //Nedbank
        private async void button3_Click(object sender, EventArgs e)
        {
            await InfoMessage("Process Started for Nedbank login");

            OnlineBankingProcessor onlineBankingProcessor = new OnlineBankingProcessor();

            await onlineBankingProcessor.NedbankLogin();
        }

        //Capitec
        private async void button4_Click(object sender, EventArgs e)
        {
            await InfoMessage("Process Started for Capitec login");

            OnlineBankingProcessor onlineBankingProcessor = new OnlineBankingProcessor();

            await onlineBankingProcessor.CapitecLogin();
        }

        //Standardbank
        private async void button5_Click(object sender, EventArgs e)
        {
            await InfoMessage("Process Started for Standardbank login");

            OnlineBankingProcessor onlineBankingProcessor = new OnlineBankingProcessor();

            await onlineBankingProcessor.StandardbankLogin();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            RichTextBox richTextBox = (RichTextBox)sender;

            richTextBox.Font = new Font("Consolas", 10f, FontStyle.Regular);
            richTextBox.BackColor = Color.White;
        }
    }
}
