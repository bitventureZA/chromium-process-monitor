﻿namespace Phoenix.OnlineBanking.BankMonitoringTool
{
    partial class OnlineBankingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.StandardbankButton = new System.Windows.Forms.Button();
            this.CapitecButton = new System.Windows.Forms.Button();
            this.NedbankButton = new System.Windows.Forms.Button();
            this.ABSAButton = new System.Windows.Forms.Button();
            this.FNBButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbLog
            // 
            this.rtbLog.Location = new System.Drawing.Point(13, 13);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.Size = new System.Drawing.Size(775, 438);
            this.rtbLog.TabIndex = 0;
            this.rtbLog.Text = "";
            this.rtbLog.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.groupBox1.Controls.Add(this.StandardbankButton);
            this.groupBox1.Controls.Add(this.CapitecButton);
            this.groupBox1.Controls.Add(this.NedbankButton);
            this.groupBox1.Controls.Add(this.ABSAButton);
            this.groupBox1.Controls.Add(this.FNBButton);
            this.groupBox1.Location = new System.Drawing.Point(13, 457);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(775, 142);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // StandardbankButton
            // 
            this.StandardbankButton.Location = new System.Drawing.Point(335, 57);
            this.StandardbankButton.Name = "StandardbankButton";
            this.StandardbankButton.Size = new System.Drawing.Size(84, 23);
            this.StandardbankButton.TabIndex = 4;
            this.StandardbankButton.Text = "Standardbank";
            this.StandardbankButton.UseVisualStyleBackColor = true;
            this.StandardbankButton.Click += new System.EventHandler(this.button5_Click);
            // 
            // CapitecButton
            // 
            this.CapitecButton.Location = new System.Drawing.Point(253, 57);
            this.CapitecButton.Name = "CapitecButton";
            this.CapitecButton.Size = new System.Drawing.Size(75, 23);
            this.CapitecButton.TabIndex = 3;
            this.CapitecButton.Text = "Capitec";
            this.CapitecButton.UseVisualStyleBackColor = true;
            this.CapitecButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // NedbankButton
            // 
            this.NedbankButton.Location = new System.Drawing.Point(171, 57);
            this.NedbankButton.Name = "NedbankButton";
            this.NedbankButton.Size = new System.Drawing.Size(75, 23);
            this.NedbankButton.TabIndex = 2;
            this.NedbankButton.Text = "Nedbank";
            this.NedbankButton.UseVisualStyleBackColor = true;
            this.NedbankButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // ABSAButton
            // 
            this.ABSAButton.Location = new System.Drawing.Point(89, 57);
            this.ABSAButton.Name = "ABSAButton";
            this.ABSAButton.Size = new System.Drawing.Size(75, 23);
            this.ABSAButton.TabIndex = 1;
            this.ABSAButton.Text = "ABSA";
            this.ABSAButton.UseVisualStyleBackColor = true;
            this.ABSAButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // FNBButton
            // 
            this.FNBButton.Location = new System.Drawing.Point(7, 57);
            this.FNBButton.Name = "FNBButton";
            this.FNBButton.Size = new System.Drawing.Size(75, 23);
            this.FNBButton.TabIndex = 0;
            this.FNBButton.Text = "FNB";
            this.FNBButton.UseVisualStyleBackColor = true;
            this.FNBButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 611);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rtbLog);
            this.Name = "Form1";
            this.Text = "Online Banking Monitoring Tool";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button StandardbankButton;
        private System.Windows.Forms.Button CapitecButton;
        private System.Windows.Forms.Button NedbankButton;
        private System.Windows.Forms.Button ABSAButton;
        private System.Windows.Forms.Button FNBButton;
    }
}

