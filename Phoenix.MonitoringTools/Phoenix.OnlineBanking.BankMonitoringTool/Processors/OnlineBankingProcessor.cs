﻿using Newtonsoft.Json;
using Phoenix.OnlineBanking.BankMonitoringTool.Core.DTOs;
using Phoenix.OnlineBanking.BankMonitoringTool.Core.DTOs.Bank;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Phoenix.OnlineBanking.BankMonitoringTool.Core.Constants;
using MediaType = Phoenix.OnlineBanking.BankMonitoringTool.Core.Constants.MediaType;
using static Phoenix.OnlineBanking.BankMonitoringTool.OnlineBankingForm;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Net;

namespace Phoenix.OnlineBanking.BankMonitoringTool.Processors
{
    public interface IOnlineBankingProcessor
    {
        Task<LoginResponse> FNBLogin();

        Task<LoginResponse> ABSALogin();

        Task<LoginResponse> NedbankLogin();

        Task<LoginResponse> CapitecLogin();

        Task<LoginResponse> StandardbankLogin();
    }

    public class OnlineBankingProcessor : IOnlineBankingProcessor
    {
        public async Task<LoginResponse> ABSALogin()
        {
            LoginResponse response = new LoginResponse();

            AuthenticationResponse authenticationResponse = new AuthenticationResponse();

            ABSARequest request = new ABSARequest()
            {
                bank = ABSACredentials.bank,
                accountNumber = ABSACredentials.AccountNumber,
                pin = ABSACredentials.Pin,
                userNumber = ABSACredentials.UserNumber,
                absolutePassword = ABSACredentials.AbsolutePassword
            };

            String serializedRequest = JsonConvert.SerializeObject(request);

            try
            {
                using (HttpClient apiClient = new HttpClient())
                {
                    #region Get Token
                    var formData = new List<KeyValuePair<string, string>>
                    {
                    new KeyValuePair<string, string>(AuthenticationHeaders.GRANT_TYPE, "password"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.USERNAME, "jordanb"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.PASSWORD, "2225D500-7012-429B-AF28-01891A7B4E15"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.SCOPE, "online-banking-api offline_access"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_ID, "online-banking-service"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_SECRET, "EEB66538-CEC2-4ADA-8CCD-D87E2F28217C")
                    };

                    var formContent = new FormUrlEncodedContent(formData);

                    var httpResponseMessage = await apiClient.PostAsync(URLs.AUTH_SERVER_REQUEST_URL, formContent);

                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        var messageContents = await httpResponseMessage.Content.ReadAsStringAsync();

                        authenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(messageContents);
                    }

                    apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authenticationResponse.Access_Token);
                    #endregion

                    await InfoMessage("Sending the request");

                    HttpResponseMessage httpResponse = await apiClient.PostAsync(URLs.Login, new StringContent(serializedRequest, Encoding.UTF8, MediaType.APPLICATION_JSON));

                    await InfoMessage("Request sent.");

                    if (!httpResponse.IsSuccessStatusCode)
                    {
                        await InfoMessage("Received code " + httpResponse.StatusCode + " trying to contact the Online Banking API, the request sent was: " + serializedRequest);
                    }
                    else
                    {
                        response = JsonConvert.DeserializeObject<LoginResponse>(await httpResponse.Content.ReadAsStringAsync());
                        await InfoMessage("The response returned "+ httpResponse.StatusCode + "");
                    }
                }
            }
            catch (Exception ex)
            {
                await ErrorMessage(ex);
            }

            await InfoMessage("Process completed for ABSA");

            return response;
        }

        public async Task<LoginResponse> CapitecLogin()
        {
            LoginResponse response = new LoginResponse();

            AuthenticationResponse authenticationResponse = new AuthenticationResponse();

            CapitecRequest request = new CapitecRequest()
            {
                bank = CapitecCredentials.bank,
                accountNumber = CapitecCredentials.AccountNumber,
                password = CapitecCredentials.Password,
                
            };

            String serializedRequest = JsonConvert.SerializeObject(request);

            try
            {
                using (HttpClient apiClient = new HttpClient())
                {
                    #region Get Token
                    var formData = new List<KeyValuePair<string, string>>
                    {
                    new KeyValuePair<string, string>(AuthenticationHeaders.GRANT_TYPE, "password"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.USERNAME, "jordanb"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.PASSWORD, "2225D500-7012-429B-AF28-01891A7B4E15"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.SCOPE, "online-banking-api offline_access"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_ID, "online-banking-service"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_SECRET, "EEB66538-CEC2-4ADA-8CCD-D87E2F28217C")
                    };

                    var formContent = new FormUrlEncodedContent(formData);

                    var httpResponseMessage = await apiClient.PostAsync(URLs.AUTH_SERVER_REQUEST_URL, formContent);

                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        var messageContents = await httpResponseMessage.Content.ReadAsStringAsync();

                        authenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(messageContents);
                    }

                    apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authenticationResponse.Access_Token);
                    #endregion

                    await InfoMessage("Sending the request");

                    HttpResponseMessage httpResponse = await apiClient.PostAsync(URLs.Login, new StringContent(serializedRequest, Encoding.UTF8, MediaType.APPLICATION_JSON));

                    await InfoMessage("Request sent.");

                    if (!httpResponse.IsSuccessStatusCode)
                    {
                        await InfoMessage("Received code " + httpResponse.StatusCode + " trying to contact the Online Banking API, the request sent was: " + serializedRequest);
                    }
                    else
                    {
                        response = JsonConvert.DeserializeObject<LoginResponse>(await httpResponse.Content.ReadAsStringAsync());
                        await InfoMessage("The response returned " + httpResponse.StatusCode + "");
                    }
                }
            }
            catch (Exception ex)
            {
                await ErrorMessage(ex);
            }

            await InfoMessage("Process completed for Capitec");

            return response;
        }

        public async Task<LoginResponse> FNBLogin()
        {
            LoginResponse response = new LoginResponse();

            AuthenticationResponse authenticationResponse = new AuthenticationResponse();

            FNBRequest request = new FNBRequest()
            {
                bank = FNBCredentials.bank,
                username = FNBCredentials.Username,
                password = FNBCredentials.Password
            };

            String serializedRequest = JsonConvert.SerializeObject(request);

            try
            {
                using (HttpClient apiClient = new HttpClient())
                {
                    #region Get Token
                    var formData = new List<KeyValuePair<string, string>>
                    {
                    new KeyValuePair<string, string>(AuthenticationHeaders.GRANT_TYPE, "password"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.USERNAME, "jordanb"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.PASSWORD, "2225D500-7012-429B-AF28-01891A7B4E15"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.SCOPE, "online-banking-api offline_access"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_ID, "online-banking-service"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_SECRET, "EEB66538-CEC2-4ADA-8CCD-D87E2F28217C")
                    };

                    var formContent = new FormUrlEncodedContent(formData);

                    var httpResponseMessage = await apiClient.PostAsync(URLs.AUTH_SERVER_REQUEST_URL, formContent);

                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        var messageContents = await httpResponseMessage.Content.ReadAsStringAsync();

                        authenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(messageContents);
                    }

                    apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authenticationResponse.Access_Token);
                    #endregion

                    await InfoMessage("Sending the request");

                    HttpResponseMessage httpResponse = await apiClient.PostAsync(URLs.Login, new StringContent(serializedRequest, Encoding.UTF8, MediaType.APPLICATION_JSON));

                    await InfoMessage("Request sent.");

                    if (!httpResponse.IsSuccessStatusCode)
                    {
                        await InfoMessage("Received code " + httpResponse.StatusCode + " trying to contact the Online Banking API, the request sent was: " + serializedRequest);
                    }
                    else
                    {
                        response = JsonConvert.DeserializeObject<LoginResponse>(await httpResponse.Content.ReadAsStringAsync());
                        await InfoMessage("The response returned " + httpResponse.StatusCode + "");
                    }
                }
            }
            catch (Exception ex)
            {
                await ErrorMessage(ex);
            }

            await InfoMessage("Process completed for FNB");

            return response;
        }

        public async Task<LoginResponse> NedbankLogin()
        {
            LoginResponse response = new LoginResponse();

            AuthenticationResponse authenticationResponse = new AuthenticationResponse();

            Core.DTOs.Bank.OldProfile oldProfile = new Core.DTOs.Bank.OldProfile()
            {
                profileType = NedbankOldCredentials.Profile.ProfileType,
                accountNumber = NedbankOldCredentials.Profile.AccountNumber,
                accountPin = NedbankOldCredentials.Profile.AccountPin,
                password = NedbankOldCredentials.Profile.Password
            };

            Core.DTOs.Bank.NewProfile newProfile = new Core.DTOs.Bank.NewProfile()
            {
                profileType = NedbankIDCredentials.Profile.ProfileType,
                username = NedbankIDCredentials.Profile.Username,
                password = NedbankIDCredentials.Profile.Password
            };

            NedbankOldRequest request = new NedbankOldRequest()
            {
                bank = NedbankOldCredentials.bank,
                profile = oldProfile
            };

            NedbankNewRequest newRequest = new NedbankNewRequest()
            {
                bank = NedbankIDCredentials.bank,
                profile = newProfile
            };

            String serializedRequestNedbankOld = JsonConvert.SerializeObject(request);

            String serializedRequestNedbankID = JsonConvert.SerializeObject(newRequest);

            try
            {
                using (HttpClient apiClient = new HttpClient())
                {
                    #region Get Token
                    var formData = new List<KeyValuePair<string, string>>
                    {
                    new KeyValuePair<string, string>(AuthenticationHeaders.GRANT_TYPE, "password"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.USERNAME, "jordanb"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.PASSWORD, "2225D500-7012-429B-AF28-01891A7B4E15"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.SCOPE, "online-banking-api offline_access"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_ID, "online-banking-service"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_SECRET, "EEB66538-CEC2-4ADA-8CCD-D87E2F28217C")
                    };

                    var formContent = new FormUrlEncodedContent(formData);

                    var httpResponseMessage = await apiClient.PostAsync(URLs.AUTH_SERVER_REQUEST_URL, formContent);

                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        var messageContents = await httpResponseMessage.Content.ReadAsStringAsync();

                        authenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(messageContents);
                    }

                    apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authenticationResponse.Access_Token);
                    #endregion

                    #region Nedbank Old
                    await InfoMessage("Sending the request for Nedbank old");

                    HttpResponseMessage httpResponse = await apiClient.PostAsync(URLs.Login, new StringContent(serializedRequestNedbankOld, Encoding.UTF8, MediaType.APPLICATION_JSON));

                    await InfoMessage("Request sent.");

                    if (!httpResponse.IsSuccessStatusCode)
                    {
                        await InfoMessage("Received code " + httpResponse.StatusCode + " trying to contact the Online Banking API, the request sent was: " + serializedRequestNedbankOld);
                    }
                    else
                    {
                        response = JsonConvert.DeserializeObject<LoginResponse>(await httpResponse.Content.ReadAsStringAsync());
                        await InfoMessage("The response returned " + httpResponse.StatusCode + " for Nedbank old");
                    }
                    #endregion

                    #region Nedbank ID
                    await InfoMessage("Sending the request for Nedbank ID");

                    HttpResponseMessage httpResponses = await apiClient.PostAsync(URLs.Login, new StringContent(serializedRequestNedbankID, Encoding.UTF8, MediaType.APPLICATION_JSON));

                    await InfoMessage("Request sent.");

                    if (!httpResponses.IsSuccessStatusCode)
                    {
                        await InfoMessage("Received code " + httpResponses.StatusCode + " trying to contact the Online Banking API, the request sent was: " + serializedRequestNedbankID);
                    }
                    else
                    {
                        response = JsonConvert.DeserializeObject<LoginResponse>(await httpResponses.Content.ReadAsStringAsync());
                        await InfoMessage("The response returned " + httpResponses.StatusCode + " for Nedbank ID");
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                await ErrorMessage(ex);
            }

            await InfoMessage("Process completed for Nedbank");

            return response;
        }

        public async Task<LoginResponse> StandardbankLogin()
        {
            LoginResponse response = new LoginResponse();

            AuthenticationResponse authenticationResponse = new AuthenticationResponse();

            StandardbankRequest request = new StandardbankRequest()
            {
                bank = StandardbankCredntials.bank,
                email = StandardbankCredntials.Email,
                password = StandardbankCredntials.Password
            };

            String serializedRequest = JsonConvert.SerializeObject(request);

            try
            {
                using (HttpClient apiClient = new HttpClient())
                {
                    #region Get Token
                    var formData = new List<KeyValuePair<string, string>>
                    {
                    new KeyValuePair<string, string>(AuthenticationHeaders.GRANT_TYPE, "password"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.USERNAME, "jordanb"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.PASSWORD, "2225D500-7012-429B-AF28-01891A7B4E15"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.SCOPE, "online-banking-api offline_access"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_ID, "online-banking-service"),
                    new KeyValuePair<string, string>(AuthenticationHeaders.CLIENT_SECRET, "EEB66538-CEC2-4ADA-8CCD-D87E2F28217C")
                    };

                    var formContent = new FormUrlEncodedContent(formData);

                    var httpResponseMessage = await apiClient.PostAsync(URLs.AUTH_SERVER_REQUEST_URL, formContent);

                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        var messageContents = await httpResponseMessage.Content.ReadAsStringAsync();

                        authenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(messageContents);
                    }

                    apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authenticationResponse.Access_Token);
                    #endregion

                    await InfoMessage("Sending the request");

                    HttpResponseMessage httpResponse = await apiClient.PostAsync(URLs.Login, new StringContent(serializedRequest, Encoding.UTF8, MediaType.APPLICATION_JSON));

                    await InfoMessage("Request sent.");

                    if (!httpResponse.IsSuccessStatusCode)
                    {
                        await InfoMessage("Received code " + httpResponse.StatusCode + " trying to contact the Online Banking API, the request sent was: " + serializedRequest);
                    }
                    else
                    {
                        response = JsonConvert.DeserializeObject<LoginResponse>(await httpResponse.Content.ReadAsStringAsync());
                        await InfoMessage("The response returned " + httpResponse.StatusCode + "");
                    }
                }
            }
            catch (Exception ex)
            {
                await ErrorMessage(ex);
            }

            await InfoMessage("Process completed for Standardbank");

            return response;
        }
    }
}
