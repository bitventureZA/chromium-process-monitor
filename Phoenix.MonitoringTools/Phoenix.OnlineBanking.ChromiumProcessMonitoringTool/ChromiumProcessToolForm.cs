﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Constants;
using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Repositories;
using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Processors;
using Quartz;
using Quartz.Impl;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool
{
    public partial class ChromiumProcessToolForm : Form
    {
        private IScheduler scheduler;
        private static FormRichTextBox Writer;


        public ChromiumProcessToolForm()
        {
            InitializeComponent();

            Writer = new FormRichTextBox(rtbLog);
            Console.SetOut(Writer);

            GetSchedulers();
        }

        //Gets the Scheduler
        public async void GetSchedulers()
        {
            scheduler = await (new StdSchedulerFactory().GetScheduler());
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Start_Button_Click(object sender, EventArgs e)
        {
            Stop_Button.Enabled = !Start_Button.Enabled;

            Writer.WriteLine("***Chromium Process Monitoring Tool***");

            ConfigureAndStartScheduler();
        }

        private void Stop_Button_Click(object sender, EventArgs e)
        {
            Start_Button.Enabled = !Stop_Button.Enabled;

            scheduler.Shutdown();

            Writer.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "]" + " Monitoring Tool Stopped.");
        }

        public void ConfigureAndStartScheduler()
        {
            scheduler.Clear();

            ISet<ITrigger> Logout = new HashSet<ITrigger>();

            IJobDetail BankLogout = JobBuilder.Create<UpdateLogoutJob>()
                .WithIdentity("UpdateLogoutJob", "UpdateBankLogouts")
                .Build();

            ITrigger UpdateLogoutTrigger = TriggerBuilder.Create()
                .WithIdentity("UpdateLogoutTrigger", "UpdateBankLogouts")
                .WithCronSchedule("0 0/1 * 1/1 * ? *")
                .Build();

            Logout.Add(UpdateLogoutTrigger);

            scheduler.Start();

            scheduler.ScheduleJob(BankLogout, UpdateLogoutTrigger);

            Writer.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "]" + " Monitoring Tool Started.");
        }

        //The Job it must execute
        [DisallowConcurrentExecution]
        public class UpdateLogoutJob : IJob
        {
            public async Task Execute(IJobExecutionContext context)
            {
                SessionRepository sessionRepository = new SessionRepository();
                KillChromiumProcessor killChromiumProcessor = new KillChromiumProcessor(sessionRepository);

                try
                {
                    await killChromiumProcessor.KillChromiumProcess();
                }
                catch (Exception ex)
                {
                    await ErrorMessage(ex);
                }
            }
        }

        //Log info Message
        public static async Task InfoMessage(string value)
        {
            Writer.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "] " + value);
        }

        //Log Error Message
        public static async Task ErrorMessage(Exception value)
        {
            Writer.WriteLine("[" + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "] " + value);
        }

        private void TextBox(object sender, EventArgs e)
        {
            RichTextBox richTextBox = (RichTextBox)sender;

            richTextBox.Font = new Font("Consolas", 10f, FontStyle.Regular);
            richTextBox.BackColor = Color.White;
        }
    }
}
