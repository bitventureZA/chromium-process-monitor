﻿using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Constants;
using PuppeteerSharp;
using System;
using System.Threading.Tasks;
using static Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Enums.Enum;
using static Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.ChromiumProcessToolForm;
using static Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Constants.Constants;
using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Repositories;
using System.Linq;
using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Models;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Processors
{
    public class KillChromiumProcessor
    {
        private readonly ISessionRepository SessionRepository;

        public static string ConnectionString;

        public KillChromiumProcessor(ISessionRepository sessionRepository)
        {
            SessionRepository = sessionRepository;
        }

        public async Task KillChromiumProcess()
        {
            DatabaseProcessor databaseProcessor = new DatabaseProcessor(SessionRepository);

            await InfoMessage("Process Started.");

            var bankUrls = (await databaseProcessor.GetOnlineBankingSessions()).ToList();

            if (bankUrls.Count == 0)
            {
                await InfoMessage("*_* There is no bank to connect to. *_* " + "Process Succesfully Completed.");
            }
            else
            {
                try
                {
                    foreach (var bankSessions in bankUrls)
                    {
                        await InfoMessage("There are " + bankUrls.Count + " session(s) open.");

                        if (Bank.Capitec.Equals(EnumHelpers.GetValueFromDescription<Bank>(bankSessions.BankCode)))
                        {
                            CleanUpCapitec(bankSessions, IP);
                        }
                        else if (Bank.NedBank.Equals(EnumHelpers.GetValueFromDescription<Bank>(bankSessions.BankCode)))
                        {
                            CleanUpNedbank(bankSessions, IP);
                        }
                        else if (Bank.FNB.Equals(EnumHelpers.GetValueFromDescription<Bank>(bankSessions.BankCode)))
                        {
                            CleanUpFNB(bankSessions, IP);
                        }
                        else if (Bank.ABSA.Equals(EnumHelpers.GetValueFromDescription<Bank>(bankSessions.BankCode)))
                        {
                            CleanUpABSA(bankSessions, IP);
                        }
                        else if (Bank.StandardBank.Equals(EnumHelpers.GetValueFromDescription<Bank>(bankSessions.BankCode)))
                        {
                            CleanUpSBSA(bankSessions, IP);
                        }
                    }
                }
                catch (Exception ex)
                {
                    await ErrorMessage(ex);

                    throw ex;
                }
            }
        }

        private static async void CleanUpFNB(Sessions bankSessions, string ip)
        {
            string url = bankSessions.PuppeteerURL;

            await InfoMessage("Looking for element.");

            Browser browser = await Puppeteer.ConnectAsync(new ConnectOptions() { BrowserWSEndpoint = url.Replace(ip, "localhost") });

            Page[] page = await browser.PagesAsync();

            ElementHandle fnb = await page[0].QuerySelectorAsync("#slide1 > div > div > div > h4");

            bool bankCodeIsFNB = (fnb != null);

            if (bankCodeIsFNB)
            {
                await InfoMessage("Busy Logging out of FNB.");

                browser.Dispose();

                DatabaseProcessor.IsDisposed(bankSessions);
            }
        }

        private static async void CleanUpABSA(Sessions bankSessions, string ip)
        {
            string url = bankSessions.PuppeteerURL;

            await InfoMessage("Looking for element.");

            Browser browser = await Puppeteer.ConnectAsync(new ConnectOptions() { BrowserWSEndpoint = url.Replace(ip, "localhost") });

            Page[] page = await browser.PagesAsync();

            ElementHandle absa = await page[0].QuerySelectorAsync("#\\30 1869976422 > div > div.richText.component.section.default-style.first.odd.margin150 > div > div > h1");

            bool bankCodeIsABSA = (absa != null);

            if (bankCodeIsABSA)
            {
                await InfoMessage("Busy Logging out of ABSA.");

                browser.Dispose();

                DatabaseProcessor.IsDisposed(bankSessions);
            }
        }

        private static async void CleanUpSBSA(Sessions bankSessions, string ip)
        {
            string url = bankSessions.PuppeteerURL;

            await InfoMessage("Looking for element.");

            Browser browser = await Puppeteer.ConnectAsync(new ConnectOptions() { BrowserWSEndpoint = url.Replace(ip, "localhost") });

            Page[] page = await browser.PagesAsync();

            ElementHandle standardbank = await page[0].QuerySelectorAsync("#expanderCard61 > div.expander-head.login-expander-header > h3");

            bool bankCodeIsStandardbank = (standardbank != null);

            if (bankCodeIsStandardbank)
            {
                await InfoMessage("Busy Logging out of Standardbank.");

                browser.Dispose();

                DatabaseProcessor.IsDisposed(bankSessions);
            }
        }

        private static async void CleanUpNedbank(Sessions bankSessions, string ip)
        {
            string url = bankSessions.PuppeteerURL;

            await InfoMessage("Looking for element.");

            Browser browser = await Puppeteer.ConnectAsync(new ConnectOptions() { BrowserWSEndpoint = url.Replace(ip, "localhost") });

            Page[] page = await browser.PagesAsync();

            ElementHandle nedbanknew = await page[0].QuerySelectorAsync("body > app-root");

            bool bankCodeIsNedbankNew = (nedbanknew != null);

            ElementHandle nedbankOld = await page[0].QuerySelectorAsync("#lblGoodBye");

            bool bankCodeIsNedbankOld = (nedbankOld != null);

            if (bankCodeIsNedbankNew)
            {
                await InfoMessage("Busy Logging out of NedbankID.");

                browser.Dispose();

                DatabaseProcessor.IsDisposed(bankSessions);
            }
            else if (bankCodeIsNedbankOld)
            {
                await InfoMessage("Busy Logging out of NedbankOld.");

                browser.Dispose();

                DatabaseProcessor.IsDisposed(bankSessions);
            }
        }

        private static async void CleanUpCapitec(Sessions bankSessions, string ip)
        {
            string url = bankSessions.PuppeteerURL;

            await InfoMessage("Looking for element.");

            Browser browser = await Puppeteer.ConnectAsync(new ConnectOptions() { BrowserWSEndpoint = url.Replace(ip, "localhost") });

            Page[] page = await browser.PagesAsync();

            ElementHandle framehandle = await page[0].QuerySelectorAsync("[name=base]");
            var frame = await framehandle.ContentFrameAsync();

            ElementHandle capitec = await frame.QuerySelectorAsync("body > div.centerPage > table:nth-child(1) > tbody > tr > td");

            bool bankCodeIsCapitec = (capitec != null);

            if (bankCodeIsCapitec)
            {
                await InfoMessage("Busy Logging out of Capitec.");

                browser.Dispose();

                DatabaseProcessor.IsDisposed(bankSessions);
            }
        }
    }
}
