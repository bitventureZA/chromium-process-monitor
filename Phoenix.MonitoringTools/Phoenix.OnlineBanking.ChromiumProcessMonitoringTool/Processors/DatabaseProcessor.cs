﻿using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Models;
using Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using static Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.ChromiumProcessToolForm;
using static Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Core.Constants.Constants;

namespace Phoenix.OnlineBanking.ChromiumProcessMonitoringTool.Processors
{
    public class DatabaseProcessor
    {
        private readonly ISessionRepository SessionRepository;

        public DatabaseProcessor(ISessionRepository sessionRepository)
        {
            SessionRepository = sessionRepository;
        }

        public async Task<IEnumerable<Sessions>> GetOnlineBankingSessions()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                await InfoMessage("Getiing all open sessions from the Session table.");

                connection.Open();
                
                IDbTransaction transaction = connection.BeginTransaction();
                    
                var bankSessions = await SessionRepository.SelectAllBankSessions(transaction);

                await InfoMessage("All sessions successfully retrieved.");

                return bankSessions;
            }
        }


        public static async void IsDisposed(Sessions bank)
        {
            SessionRepository sessionRepository = new SessionRepository();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                await InfoMessage("Updating Session table.");

                connection.Open();

                IDbTransaction transaction = connection.BeginTransaction();
                
                var i = await sessionRepository.UpdateSessionTable(transaction, bank);

                await InfoMessage("Process Successfully Completed.");
            }
        }
    }
}
